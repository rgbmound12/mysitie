# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Admin(models.Model):
    idadmin = models.IntegerField(primary_key=True)
    idusuario = models.IntegerField(blank=True, null=True)
    codigodeadmin = models.IntegerField(blank=True, null=True)
    nombredeadmin = models.CharField(max_length=20, blank=True, null=True)
    idcontacto = models.ForeignKey('Contactos', models.DO_NOTHING, db_column='idcontacto', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin'


class Comentarios(models.Model):
    idcomentario = models.IntegerField(primary_key=True)
    idusuario = models.ForeignKey('Usuario', models.DO_NOTHING, db_column='idusuario', blank=True, null=True)
    codigodecomentario = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'comentarios'


class Contactos(models.Model):
    idcontacto = models.IntegerField(primary_key=True)
    idusuario = models.ForeignKey(Comentarios, models.DO_NOTHING, db_column='idusuario', blank=True, null=True)
    contactosmoderador = models.CharField(max_length=20, blank=True, null=True)
    contactosdecreador = models.CharField(max_length=20, blank=True, null=True)
    contactosdeadmin = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'contactos'


class Creador(models.Model):
    idcreador = models.IntegerField(primary_key=True)
    idusuario = models.IntegerField(blank=True, null=True)
    nombredecreador = models.CharField(max_length=20, blank=True, null=True)
    idcontacto = models.ForeignKey(Contactos, models.DO_NOTHING, db_column='idcontacto', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'creador'


class Iniciodesesion(models.Model):
    idusuario = models.IntegerField(primary_key=True)
    idusuarioxpermiso = models.IntegerField(blank=True, null=True)
    contraseña = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'iniciodesesion'


class Moderador(models.Model):
    idmoderador = models.IntegerField(primary_key=True)
    idusuario = models.IntegerField(blank=True, null=True)
    idcodigodemoderador = models.IntegerField(blank=True, null=True)
    idcontacto = models.ForeignKey(Contactos, models.DO_NOTHING, db_column='idcontacto', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'moderador'


class Permiso(models.Model):
    idusuarioxpermiso = models.IntegerField(primary_key=True)
    idusuario = models.ForeignKey(Iniciodesesion, models.DO_NOTHING, db_column='idusuario', blank=True, null=True)
    codigodepermiso = models.CharField(max_length=20, blank=True, null=True)
    idadmin = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'permiso'


class Publicaciones(models.Model):
    idpublicacion = models.IntegerField(primary_key=True)
    idcodigodepublicacion = models.IntegerField(blank=True, null=True)
    idusuario = models.ForeignKey('Usuario', models.DO_NOTHING, db_column='idusuario', blank=True, null=True)
    nombredeusuario = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'publicaciones'


class Usuario(models.Model):
    idusuario = models.OneToOneField(Permiso, models.DO_NOTHING, db_column='idusuario', primary_key=True)
    nombredeusuario = models.CharField(max_length=20, blank=True, null=True)
    codigodeusuario = models.CharField(max_length=20, blank=True, null=True)
    contraseña = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usuario'
